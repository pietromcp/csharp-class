﻿using System;
using System.Windows.Forms;

namespace WinFormsMemoryLeaks
{
    public class Clock : Label
    {
        private readonly Timer timer;

        public Clock() {
            timer = new Timer {
                Enabled = true,
                Interval = 500
            };
            timer.Tick += ClockTick;
        }

        private void ClockTick(object sender, EventArgs e)
        {
            Text = DateTimeOffset.Now.ToString();
        }

        public void Stop()
        {
            timer.Enabled = false;
            timer.Tick -= ClockTick;
        }
    }
}
