﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace WinFormsMemoryLeaks
{
    public partial class MemoryLeaksApp : Form
    {
        public MemoryLeaksApp()
        {
            InitializeComponent();
            CustomizeComponent();
        }

        private readonly ICollection<Clock> clocks = new List<Clock>();

        private void CustomizeComponent()
        {}

        const int clockCount = 50;

        private void start_Click(object sender, EventArgs e) {
            for (int i = 0; i < clockCount; i++)
            {
                var clock = new Clock {
                    Left = (i * 100) % Width,
                    Height = 50 * ((i * 100) / Width)
                };
                clocks.Add(clock);
                Controls.Add(clock);
            }
        }

        private void stop_Click(object sender, EventArgs e)
        {
            foreach (var clock in clocks)
            {
                Controls.Remove(clock);
                clock.Stop();
            }
            clocks.Clear();
        }
    }
}
