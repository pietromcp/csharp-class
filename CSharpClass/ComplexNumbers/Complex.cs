﻿using System;

namespace CSharpClass.ComplexNumbers
{
    public class Complex
    {
        public double Re { get; private set; }
        public double Im { get; private set; }

        public Complex(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public override  string ToString()
        {
            return $"({Re} + {Im}i)" + base.ToString();
        }

        public static Complex operator +(Complex x, Complex y)
        {
            return new Complex(x.Re + y.Re, x.Im + y.Im);
        }

        public static Complex operator -(Complex x, Complex y)
        {
            return new Complex(x.Re - y.Re, x.Im - y.Im);
        }

        public static Complex operator -(Complex x)
        {
            return new Complex(-x.Re, -x.Im);
        }

        public static Complex operator *(Complex x, Complex y)
        {
            return new Complex(x.Re * y.Re - x.Im * y.Im, x.Re * y.Im + x.Im * y.Re);
        }

        //public static Complex operator *(int n, Complex c) {
        //    return new Complex(n * c.Re, n * c.Im);
        //}

        //public static Complex operator *(Complex c, int n)
        //{
        //    return n * c;
        //}

        public static implicit operator string(Complex c)
        {
            return c.ToString();
        }

        public static implicit operator Complex(int n) => new Complex(n, 0);

        public static explicit operator Complex(string s)
        {
            string[] fields = s.Split('+');
            double re = double.Parse(fields[0].Trim());
            double im = double.Parse(fields[1].Replace("i", "").Trim());
            return new Complex(re, im);
        }

        public static void DoSample()
        {
            Complex a = new Complex(2, 3);
            Complex b = new Complex(5, 10);
            Complex sum = a + b;
            Console.WriteLine($"{a} + {b} = {sum}");
            Complex difference = a - b;
            Console.WriteLine($"{b} - {a} = {difference}");
            Console.WriteLine($"-{a} = {-a}");
            Console.WriteLine($"3 * {a} = { 3 * a }");
            Console.WriteLine($"{a} * 3 = { a * 3 }");
            string asString = a;
            Console.WriteLine($"As String: {asString}");
            Complex backAsComplex = (Complex)"3 + 8i";
            Console.WriteLine($"Back as complex: {backAsComplex.Re}, {backAsComplex.Im}");

            A x = new A { Value = 19 };
            B y = x;

        }
    }

    class A
    {
        public int Value { get; set; }

        public static explicit operator B(A a)
        {
            return new B { Value = -a.Value };
        }
    }

    class B
    {
        public int Value { get; set; }

        public static implicit operator B(A a)
        {
            return new B { Value = -a.Value };
        }
    }
}