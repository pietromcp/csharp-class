﻿using System;
using System.Collections.Generic;
//using System.Collections;
using CSharpClass.Counting;
using CSharpClass.Indexers;
using CSharpClass.ExtMethods;
using CSharpClass.Delegates;
using CSharpClass.ComplexNumbers;
using CSharpClass.Inheritance;


namespace CSharpClass
{
    class X { }
    class Program
    {
        static void Main(string[] args)
        {
            //GenericsSample.Run();

            CSharpClass.Partial.PartialSample.DoSample();
            //WordCounter.Bar();
            //DelegatesSample.UseDelegates();
            //InheritanceSamples.DoSamples();
            //Complex.DoSample();
            //EventsSampleHandmade.DoSample();
            //DelegatesSample.UseFunctions();
            //ExtMethodsSamples.DoSamples();
            //WordCounter.Foo();

            //Counter cnt0 = new Counter();
            //Console.WriteLine(cnt0.Value);
            //Counter cnt1 = new Counter(19);
            //Console.WriteLine(cnt1.Value);
            ////X x1 = null;
            //X x2 = null;
            //Console.WriteLine("Non valori " + (x1 == x2));

            //int x = 19;
            //System.Int32 y = 11;
            //Int32 z = 6;
            //Console.WriteLine("Hello, World!");
            //StringSamples.DoSamples();
            //BitABitSamples.DoSamples();
            //UseClasses();
            //UseList();
            //Parameters.Simple.DoSimpleTest();
            //Parameters.Optional.DoTest();
            //Parameters.Varargs.DoTest();
            //Parameters.ByName.DoTest();
            //Parameters.Out.DoTest();
            //string valueStr = "a19";
            //int value;
            //bool result = int.TryParse(valueStr, out value);
            //Console.WriteLine(result + " " + value);
            //Parameters.Ref.DoTest();
            //This.Constructors.DoTests();
            Console.ReadKey();
        }

        //static void Main()
        //{
        //    Console.WriteLine("Hello, World (no args)!");
        //    Console.ReadKey();
        //}

        //static void Main(string[] args)
        //{
        //    Console.WriteLine("Hello, World!");
        //    Console.WriteLine(args.Length);
        //    Console.WriteLine(args);
        //    foreach (string arg in args)
        //    {
        //        Console.WriteLine(arg);
        //    }
        //    Console.ReadKey();
        //}

        //static void Main(string[] args)
        //{
        //    Console.WriteLine("Hello, World!");
        //    Console.WriteLine(args.Length);
        //    Console.WriteLine(args);
        //    for (int i = 0; i < args.Length; i++)
        //    {
        //        string s = string.Format("{0} {1}", i, args[i]);
        //        Console.WriteLine(s);
        //    }
        //    Console.ReadKey();
        //}

        //static void Main(string[] args)
        //{
        //    Console.WriteLine("Hello, World!");
        //    Console.WriteLine(args.Length);
        //    Console.WriteLine(args);
        //    for (int i = 0; i < args.Length; i++)
        //    {
        //        string row1 = i + " " + args[i];
        //        string row2 = string.Format("{0} {1}", i, args[i]);
        //        Console.WriteLine(row1);
        //        Console.WriteLine(row2);
        //        Console.WriteLine($"{i} {args[i]}");
        //    }
        //    Console.ReadKey();
        //}

        static void UseClasses()
        {
            Person p = new Person("Pietro", "Martinelli");
            Console.WriteLine(p.FullName);
        }

        static void UseList()
        {
            IList<string> list = new List<string>();
            list.Add("Pippo");
            list.Add("Pluto");
            list.Add("Paperino");
            Console.WriteLine($"Second element is {list[1]}");
            Console.WriteLine($"Total items count is {list.Count}");
        }
    }

    class Person
    {
        private string firstName, lastName;
        private DateTime birthDay;

        public int Age { get; } = 19;

        internal Person(string first, string last)
        {
            firstName = first;
            lastName = last;
        }

        internal string FullName
        {
            get
            {
                return firstName + " " + lastName;
            }
            set
            {
                string[] fields = value.Split(' ');
                firstName = fields[0];
                firstName = fields[1];
            }
        }

        internal string GetFullName()
        {
            return firstName + " " + lastName;
        }

        //internal string FullName => firstName + " " + lastName;

        internal DateTime BirthDay
        {
            get { return birthDay; }
            private set
            {
                birthDay = value;
            }
        }
    }
}
