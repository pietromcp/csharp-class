﻿using System;

namespace CSharpClass.Indexers
{
    class WordCounter
    {
        public int this[string key]
        {
            get { return key.Length; }
            set { Console.WriteLine($"{key} {value}");  }
        }

        public int this[int key]
        {
            get { return key; }
            set { Console.WriteLine($"{key} {value}"); }
        }

        public int this[int key1, string key2]
        {
            get { return key1 + key2.Length; }
            set { Console.WriteLine($"{key1} {key2} {value}"); }
        }

        public int GetWithKey(string key) {
            return key.Length;
        }

        public void SetWithKey(string key, int value) {
        }

        public static void Foo()
        {
            WordCounter mc = new WordCounter();
            int n = mc["Pietro"];
            mc["Pietro"] = 19;

            mc[11] = 17;
            mc[11, "Pietro"] = 44;
        }

        public static void Bar()
        {
            string template = "[Banca]: [Filiale]";
            object parameters = new {
                Banca = "123",
                Filiale = "456"
            };
            string result = Process(template, parameters);
            Console.WriteLine(result);
        }

        private static string Process(string template, object parameters)
        {
            string result = template;
            foreach (var p in parameters.GetType().GetProperties())
            {
                string pName = p.Name;
                object pValue = p.GetValue(parameters);
                result = result.Replace($"[{pName}]", pValue.ToString());
            }
            return result;
        }
    }
}
