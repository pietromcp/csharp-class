﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpClass
{
    class GenericsSample
    {
        internal static void Run()
        {
            IList<string> strings = new List<string>();
            //strings.Add("Pippo");
            //strings.Add("Pluto");
            //strings.Add("Paperino");
            ////int i = strings[2];

            IDictionary<string, int> occurrences = new Dictionary<string, int>();


            //Repository<string, Person> peopleRepo = new FakeRepo<string, Person>();

            //peopleRepo.Save(new Person("PP", "MM"));


            MyList<string> myStrings = new MyFakeList<string>();
            MyList<object> myObjects = null;
            myObjects = myStrings;
            //myObjects.Add(19);
            //myStrings.Add("aaa");
        }
    }

    delegate void Consumer<T>(T t);

    interface MyList<out T> {
        T this[int index] { get; }

        //void Add(T value);

        //void Process(Consumer<T> consumer);
    }

    class MyFakeList<T> : MyList<T>
    {
        public T this[int index] => throw new NotImplementedException();

        public void Add(T value)
        {
        }
    }

    interface Repository<TKey, TEntity>
    {
        IEnumerable<TEntity> FindAll();

        void Save(TEntity newInstance);

        void Update(TEntity instance);

        TEntity FindById(TKey key);
    }

    class FakeRepo<K, T> : Repository<K, T>
    {
        public IEnumerable<T> FindAll()
        {
            throw new NotImplementedException();
        }

        public T FindById(K key)
        {
            throw new NotImplementedException();
        }

        public void Save(T newInstance)
        {
            throw new NotImplementedException();
        }

        public void Update(T instance)
        {
            throw new NotImplementedException();
        }
    }
}
