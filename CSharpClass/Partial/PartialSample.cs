﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpClass.Partial
{
    partial class MyClass {
        public int IValue { get; set; }

        //partial void OnSomethingHappened(string s) {
        //    Console.WriteLine(s);
        //}
    }

    
    class PartialSample
    {
        internal static void DoSample() {
            Console.WriteLine(new MyClass().DoubleValue());
        }
    }
}
