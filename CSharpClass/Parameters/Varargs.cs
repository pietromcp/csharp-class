﻿using System;

namespace CSharpClass.Parameters
{
    class Varargs
    {
        internal static void MyMethod(string s, int n = 19, params string[] items)
        {
            Console.WriteLine($"{s} {n}");
            foreach (string item in items)
            {
                Console.WriteLine(item);
            }
        }

        public static void DoTest()
        {
            //MyMethod("Pietro", items: new[] { "A", "B" });
            //MyMethod("Pietro", 20, "x", "y", "z", "aa");
            MyMethod("aaa");
            MyMethod("aaa", 11);
            MyMethod("aaa", 11, "bb");
            MyMethod("aaa", 11, "bb", "cc");
            MyMethod("aaa", 11, new[] { "x", "y", "z"});

            MyMethod("aaa", items: new[] { "bb", "cc" });
        }
    }
}
