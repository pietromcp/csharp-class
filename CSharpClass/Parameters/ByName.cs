﻿using System;

namespace CSharpClass.Parameters
{
    class ByName
    {
        internal static void MyMethod(string s, int n)
        {
            Console.WriteLine($"{s} {n}");
        }

        public static void DoTest()
        {
            MyMethod("Pietro", 19);
            MyMethod(n: 11, s: "abcde");
        }
    }
}
