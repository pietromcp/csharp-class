﻿using System;

namespace CSharpClass.Parameters
{
    class Optional
    {
        internal static void MyMethod(string s, int n = 19, double x = 11.0)
        {
            Console.WriteLine($"{s} {n} {x}");
        }

        public static void DoTest()
        {
            //MyMethod("Pietro", 3, 3);
            //MyMethod("Pietro", 20, 4);

            MyMethod("xx");
            MyMethod("xx", 1);
            MyMethod("xx", 1, 22);
            MyMethod("xx", x: 1.1);
            MyMethod("xx", x: 1.1, n: 111);
        }
    }
}
