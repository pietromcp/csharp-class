﻿using System;

namespace CSharpClass.Parameters
{
    class Ref
    {
        internal static void MyMethod(int n, ref int result)
        {
            result = 2 * n;
            n = 100;
        }

        public static void DoTest()
        {
            int n = 11;
            int res = 0;
            MyMethod(n, ref res);
            Console.WriteLine($"REF {n} {res}");
        }
    }
}
