﻿using System;

namespace CSharpClass.Parameters
{
    class Out
    {
        internal static void MyMethod(int n, out int result)
        {
            result = 2 * n;
            n = 100;
        }

        public static void DoTest()
        {
            int n = 11;
            int res;
            MyMethod(n, out res);
            Console.WriteLine($"OUT {n} {res}");
        }
    }
}
