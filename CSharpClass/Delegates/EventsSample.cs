﻿using System;
using System.Collections.Generic;
using System.Timers;

namespace CSharpClass.Delegates
{
    delegate void AlarmEvent();

    //class Alarm {
    //    public event AlarmEvent Handlers;

    //    public static void DoSample() {
    //        Alarm alarm = new Alarm(TimeSpan.FromSeconds(15));
    //        alarm.Handlers += () => { Console.WriteLine("Suona!"); };
    //        alarm.Start();

    //        Timer timer;
    //    }


    //}


    class EvtArgs
    {
        public string SValue { get; set; }
        public long LValue { get; set; }

        public override string ToString()
        {
            return $"{SValue} - {LValue}";
        }
    }

    delegate void EvtArgsHandler(EvtArgs args);

    class EventsSampleHandmade
    {
        private IList<EvtArgsHandler> handlers = new List<EvtArgsHandler>();

        public void RegisterHandler(EvtArgsHandler handler) {
            handlers.Add(handler);
        }

        public void OnRaisArgsEvent(EvtArgs args) {
            foreach (EvtArgsHandler h in handlers) {
                h(args);
            }
        }


        internal static void DoSample() {
            EventsSampleHandmade sample = new EventsSampleHandmade();
            sample.RegisterHandler((args) => { Console.WriteLine($"Handler 0 {args}"); });
            sample.RegisterHandler((args) => { Console.WriteLine($"Handler 1 {args}"); });
            sample.RegisterHandler(delegate (EvtArgs args) { Console.WriteLine($"Handler 4 {args}"); });
            sample.RegisterHandler(MethodHandler);

            sample.OnRaisArgsEvent(new EvtArgs { SValue = "TEST", LValue = 47 });
        }

        internal static void MethodHandler(EvtArgs args)
        {
            Console.WriteLine($"Method handler: {args}");
        }
    }

    class EventsSample
    {
        public event EvtArgsHandler ArgsEvent;

        public void OnRaisArgsEvent(EvtArgs args)
        {
            ArgsEvent?.Invoke(args);
        }

        public static void DoSample()
        {
            EventsSample sample = new EventsSample();
            sample.ArgsEvent += (args) => { Console.WriteLine($"Handler 0 {args}"); };
            sample.ArgsEvent += (args) => { Console.WriteLine($"Handler 1 {args}"); };
            sample.ArgsEvent += (args) => { Console.WriteLine($"Handler 2 {args}"); };
            sample.ArgsEvent += (args) => { Console.WriteLine($"Handler 3 {args}"); };
            sample.ArgsEvent += delegate(EvtArgs args) { Console.WriteLine($"Handler 4 {args}"); };
            sample.ArgsEvent += MethodHandler;

            sample.OnRaisArgsEvent(new EvtArgs { SValue = "TEST", LValue = 47 });
        }

        internal static void MethodHandler(EvtArgs args)
        {
            Console.WriteLine($"Method handler: {args}");
        }
    }
}
