﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpClass
{
    class StringSamples
    {
        public static void DoSamples()
        {
            string pietro = "Pietro";

            string pie = "Pietro";

            Console.WriteLine(pietro == pie);

            string multiLineString = @"
Hello,
my beautiful     beautiful
World!
";
            Console.WriteLine(multiLineString);

            int age = 40;
            string interpolationSample = $"My age is {age}";
            Console.WriteLine(interpolationSample);
        }
    }
}
