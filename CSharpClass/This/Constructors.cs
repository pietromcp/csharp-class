﻿using System;

namespace CSharpClass.This
{
    class Constructors
    {
        private int value;

        public Constructors(int value)
        {
            this.value = value;
        }

        public Constructors() : this(19)
        {
        }

        public void Printvalue()
        {
            Console.WriteLine($"Value is {value}");
        }

        public static void DoTests()
        {
            Constructors c1 = new Constructors(11);
            c1.Printvalue();
            Constructors c2 = new Constructors();
            c2.Printvalue();
        }
    }
}
