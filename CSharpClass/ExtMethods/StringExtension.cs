﻿namespace CSharpClass.ExtMethods
{
    public static class StringExtension
    {
        public static bool IsNullOrEmpty(this string input)
        {
            return string.IsNullOrEmpty(input);
        }

        public static bool HasValue(this string input)
        {
            return !input.IsNullOrEmpty();
        }

        public static string OrEmpty(this string input)
        {
            return input.Or("");
        }

        public static string For(this string s, int n){
            return s;
        }
    }
}
