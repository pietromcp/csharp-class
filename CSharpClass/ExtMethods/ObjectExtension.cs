﻿namespace CSharpClass.ExtMethods
{
    public static class ObjectExtension
    {
        public static T Or<T>(this T input, T defaultValue)
        {
            if (input == null)
            {
                return defaultValue;
            }
            return input;
        }

        public static TResult Or<TInput, TDefault, TResult>(this TInput input, TDefault defaultValue)
            where TInput : TResult
            where TDefault : TResult
        {
            if (input == null)
            {
                return defaultValue;
            }
            return input;
        }
    }
}
