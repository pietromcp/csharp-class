﻿using System;

namespace CSharpClass.ExtMethods
{
    public static class IntExtension
    {
        public static void Times(this int n, Action<int> action)
        {
            for (int i = 0; i < n; i++)
            {
                action(i);
            }
        }
    }
}
