﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace CSharpClass.ExtMethods
{
    internal class ExtMethodsSamples
    {
        internal static void DoSamples()
        {
            IntExtension.Times(5, i => Console.WriteLine($"--Execution {i}"));
            5.Times(i => Console.WriteLine($"Execution {i}"));

            string s = null;
            Console.WriteLine($"String is {s.OrEmpty()}!");
            Console.WriteLine($"String is {s.Or("Default")}!");
            int? n = null;
            Console.WriteLine($"N is {n.Or(123)}!");
            Console.WriteLine($"N is {((int?)null).Or(123)}!");

            IEnumerable<string> items = new[] { "Pippo", "Pluto", "Paperino" };
            items.ForEach(x => Console.WriteLine(x));
            items.ForEach(Console.WriteLine);

            IEnumerable items2 = new[] { "Pippo", "Pluto", "Paperino" };
            foreach(string x in items2) {
                Console.WriteLine(x);
            }

            var enumerator = items2.GetEnumerator();
            while (enumerator.MoveNext()) {
                Console.WriteLine(enumerator.Current);
            }

            C1 c1 = null;
            C3 c3 = c1.Or<C1, C2, C3>(new C2());
        }
    }

    internal class C3 { }

    internal class C1 : C3{ }

    internal class C2 : C3 { }
}
