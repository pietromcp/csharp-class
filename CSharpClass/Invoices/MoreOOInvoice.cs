﻿namespace CSharpClass.Invoices
{
    class MoreOOInvoice
    {
        Customer customer;

        string GetCustomerDescription()
        {
            return customer.GetDescription();
        }
    }

    class Customer
    {
        string name, vat, address;

        internal string GetDescription()
        {
            return $"{name} {vat} {address}";
        }
    }
}
