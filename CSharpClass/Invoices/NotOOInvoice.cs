﻿namespace CSharpClass.Invoices
{
    class NotOOInvoice
    {
        string customerName;
        string customerVat;
        string customerAddress;

        string GetCustomerDescription() {
            return $"{ customerName} { customerVat} { customerAddress}";
        }
    }
}
