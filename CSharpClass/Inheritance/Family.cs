﻿using System;

namespace CSharpClass.Inheritance
{
    public interface Int1 {
        void Foo();
    }

    public interface Int2
    {
        void Foo();
    }

    public class C : Int1, Int2
    {
        void Int1.Foo()
        {
            Console.WriteLine("Int1_C.Foo");
        }

        void Int2.Foo()
        {
            Console.WriteLine("Int2_C.Foo");
        }

        public void Foo() {
            Console.WriteLine("C.Foo");
        }
    }

    public class Parent
    {
        public virtual void Foo()
        {
            Console.WriteLine("Vater.Foo");
        }

        public void Bar()
        {
            Console.WriteLine("Vater.Bar");
        }
    }

    public class Son1 : Parent
    {
        public override void Foo()
        {
            Console.WriteLine("Son1.Foo");
        }

        public new void Bar()
        {
            Console.WriteLine("Son1.Bar");
        }
    }

    public class Son2 : Parent
    {
        public override sealed void Foo()
        {
            Console.WriteLine("Son2.Foo");
        }
    }

    public class Son11 : Son1
    {
        public override sealed void Foo()
        {
            Console.WriteLine("Son11.Foo");
        }
    }

    public class Son21 : Son2
    {
        //public override void Foo() // Compilation error
        //{
        //    Console.WriteLine("Son12.Foo");
        //}
    }

    public sealed class Son3 : Parent
    {
        public void Foo()
        {
            Console.WriteLine("Son3.Foo");
        }
    }

    //public class Son31 : Son3 { } // Compilation error

    public class Son4 : Parent
    {
        public new void Foo()
        {
            Console.WriteLine("Son4.Foo");
        }
    }

    public class InheritanceSamples
    {
        public static void DoSamples()
        {
            Son3 s3 = new Son3();
            Son4 s4 = new Son4();
            Parent[] items = new Parent[]
            {
                new Parent(),
                new Son1(),
                new Son2(),
                new Son11(),
                new Son21(),
                s3,
                s4
            };

            foreach (Parent p in items)
            {
                p.Foo();
                p.Bar();
            }
            s3.Foo();
            s4.Foo();
            Parent p3 = s3;
            p3.Foo();
            Son3 s3bis = items[4] as Son3;
            s3bis?.Foo();
            if (s3bis != null) {
                s3bis.Foo();
            }

            Int1 c1 = new C();
            Int2 c2 = new C();
            c1.Foo();
            c2.Foo();
            C c = new C();
            c.Foo();
        }
    }
}
