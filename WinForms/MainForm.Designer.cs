﻿using System.Drawing;

namespace WinForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.username = new System.Windows.Forms.TextBox();
            this.password = new System.Windows.Forms.TextBox();
            this.submit = new System.Windows.Forms.Button();
            this.result = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.start = new System.Windows.Forms.Button();
            this.stop = new System.Windows.Forms.Button();
            this.myLabel1 = new WinForms.MyLabel();
            this.numericBox1 = new WinForms.NumericBox();
            this.myComposedControl1 = new WinForms.MyComposedControl();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.UsernameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstNameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastNameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BirthdayCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BirthdayAsDateTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BirthdayAsDateTimeCol2 = new WinForms.DateTimeColumn();
            this.IsActiveCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.save = new System.Windows.Forms.Button();
            this.myLabel2 = new WinForms.MyLabel();
            this.myComposedControl2 = new WinForms.MyComposedControl();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // username
            // 
            this.username.Location = new System.Drawing.Point(49, 64);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(182, 22);
            this.username.TabIndex = 0;
            this.username.Text = "[Username]";
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(49, 120);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(182, 22);
            this.password.TabIndex = 1;
            this.password.Text = "[password]";
            this.password.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // submit
            // 
            this.submit.Location = new System.Drawing.Point(118, 169);
            this.submit.Name = "submit";
            this.submit.Size = new System.Drawing.Size(112, 23);
            this.submit.TabIndex = 2;
            this.submit.Text = "Submit";
            this.submit.UseVisualStyleBackColor = true;
            this.submit.Click += new System.EventHandler(this.button1_Click);
            // 
            // result
            // 
            this.result.AutoSize = true;
            this.result.BackColor = System.Drawing.SystemColors.HotTrack;
            this.result.Location = new System.Drawing.Point(49, 256);
            this.result.MaximumSize = new System.Drawing.Size(400, 0);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(0, 17);
            this.result.TabIndex = 3;
            this.result.TextChanged += new System.EventHandler(this.result_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(118, 216);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Next Form";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(234, 401);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(75, 23);
            this.start.TabIndex = 5;
            this.start.Text = "Start";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.button2_Click);
            // 
            // stop
            // 
            this.stop.Location = new System.Drawing.Point(118, 401);
            this.stop.Name = "stop";
            this.stop.Size = new System.Drawing.Size(75, 23);
            this.stop.TabIndex = 6;
            this.stop.Text = "Stop";
            this.stop.UseVisualStyleBackColor = true;
            this.stop.Click += new System.EventHandler(this.stop_Click);
            // 
            // myLabel1
            // 
            this.myLabel1.Interval = 1000;
            this.myLabel1.Location = new System.Drawing.Point(501, 63);
            this.myLabel1.Name = "myLabel1";
            this.myLabel1.Size = new System.Drawing.Size(75, 23);
            this.myLabel1.TabIndex = 7;
            this.myLabel1.Text = "myLabel1";
            // 
            // numericBox1
            // 
            this.numericBox1.Location = new System.Drawing.Point(333, 64);
            this.numericBox1.Name = "numericBox1";
            this.numericBox1.Size = new System.Drawing.Size(100, 22);
            this.numericBox1.TabIndex = 8;
            this.numericBox1.Text = "1919";
            this.numericBox1.Value = 1919D;
            this.numericBox1.TextChanged += new System.EventHandler(this.numericBox1_TextChanged);
            // 
            // myComposedControl1
            // 
            this.myComposedControl1.Location = new System.Drawing.Point(579, 21);
            this.myComposedControl1.Message = "Paperino";
            this.myComposedControl1.Name = "myComposedControl1";
            this.myComposedControl1.Size = new System.Drawing.Size(314, 193);
            this.myComposedControl1.TabIndex = 9;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UsernameCol,
            this.FirstNameCol,
            this.LastNameCol,
            this.BirthdayCol,
            this.BirthdayAsDateTimeCol,
            this.BirthdayAsDateTimeCol2,
            this.IsActiveCol});
            this.dataGridView1.Location = new System.Drawing.Point(842, 26);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(900, 247);
            this.dataGridView1.TabIndex = 10;
            // 
            // UsernameCol
            // 
            this.UsernameCol.DataPropertyName = "Username";
            this.UsernameCol.HeaderText = "Username";
            this.UsernameCol.Name = "UsernameCol";
            this.UsernameCol.ReadOnly = true;
            this.UsernameCol.Width = 102;
            // 
            // FirstNameCol
            // 
            this.FirstNameCol.DataPropertyName = "FirstName";
            this.FirstNameCol.HeaderText = "FirstName";
            this.FirstNameCol.Name = "FirstNameCol";
            this.FirstNameCol.Width = 101;
            // 
            // LastNameCol
            // 
            this.LastNameCol.DataPropertyName = "LastName";
            this.LastNameCol.HeaderText = "LastName";
            this.LastNameCol.Name = "LastNameCol";
            this.LastNameCol.ReadOnly = true;
            this.LastNameCol.Width = 101;
            // 
            // BirthdayCol
            // 
            this.BirthdayCol.DataPropertyName = "BirthdayAsDate";
            this.BirthdayCol.HeaderText = "BirthDay";
            this.BirthdayCol.Name = "BirthdayCol";
            this.BirthdayCol.Width = 91;
            // 
            // BirthdayAsDateTimeCol
            // 
            this.BirthdayAsDateTimeCol.DataPropertyName = "BirthdayAsDateTime";
            this.BirthdayAsDateTimeCol.HeaderText = "BirthdayAsDateTime";
            this.BirthdayAsDateTimeCol.Name = "BirthdayAsDateTimeCol";
            this.BirthdayAsDateTimeCol.Width = 166;
            // 
            // BirthdayAsDateTimeCol2
            // 
            this.BirthdayAsDateTimeCol2.DataPropertyName = "BirthdayAsDateTime";
            this.BirthdayAsDateTimeCol2.HeaderText = "BirthdayAsDateTime2";
            this.BirthdayAsDateTimeCol2.Name = "BirthdayAsDateTimeCol2";
            this.BirthdayAsDateTimeCol2.Width = 151;
            // 
            // IsActiveCol
            // 
            this.IsActiveCol.DataPropertyName = "IsActive";
            this.IsActiveCol.HeaderText = "IsActive";
            this.IsActiveCol.Name = "IsActiveCol";
            this.IsActiveCol.Width = 62;
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(842, 297);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(75, 23);
            this.save.TabIndex = 11;
            this.save.Text = "Save";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // myLabel2
            // 
            this.myLabel2.Interval = 5000;
            this.myLabel2.Location = new System.Drawing.Point(452, 337);
            this.myLabel2.Name = "myLabel2";
            this.myLabel2.Size = new System.Drawing.Size(589, 317);
            this.myLabel2.TabIndex = 12;
            this.myLabel2.Text = "Seconda etichetta orribile";
            // 
            // myComposedControl2
            // 
            this.myComposedControl2.Location = new System.Drawing.Point(861, 401);
            this.myComposedControl2.Message = "Topolino";
            this.myComposedControl2.Name = "myComposedControl2";
            this.myComposedControl2.Size = new System.Drawing.Size(314, 193);
            this.myComposedControl2.TabIndex = 13;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1798, 666);
            this.Controls.Add(this.myComposedControl2);
            this.Controls.Add(this.myLabel2);
            this.Controls.Add(this.save);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.myComposedControl1);
            this.Controls.Add(this.numericBox1);
            this.Controls.Add(this.myLabel1);
            this.Controls.Add(this.stop);
            this.Controls.Add(this.start);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.result);
            this.Controls.Add(this.submit);
            this.Controls.Add(this.password);
            this.Controls.Add(this.username);
            this.Name = "MainForm";
            this.Text = "MainFormText";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.Button submit;
        private System.Windows.Forms.Label result;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Button stop;
        private MyLabel myLabel1;
        private NumericBox numericBox1;
        private MyComposedControl myComposedControl1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn UsernameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstNameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastNameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn BirthdayCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn BirthdayAsDateTimeCol;
        private DateTimeColumn BirthdayAsDateTimeCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsActiveCol;
        private System.Windows.Forms.Button save;
        private MyLabel myLabel2;
        private MyComposedControl myComposedControl2;
    }
}