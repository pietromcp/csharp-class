﻿namespace WinForms
{
    partial class MyComposedControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.theCheckbox = new System.Windows.Forms.CheckBox();
            this.theLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // theCheckbox
            // 
            this.theCheckbox.AutoSize = true;
            this.theCheckbox.Location = new System.Drawing.Point(17, 21);
            this.theCheckbox.Name = "theCheckbox";
            this.theCheckbox.Size = new System.Drawing.Size(95, 21);
            this.theCheckbox.TabIndex = 0;
            this.theCheckbox.Text = "[Message]";
            this.theCheckbox.UseVisualStyleBackColor = true;
            // 
            // theLabel
            // 
            this.theLabel.AutoSize = true;
            this.theLabel.Location = new System.Drawing.Point(136, 25);
            this.theLabel.Name = "theLabel";
            this.theLabel.Size = new System.Drawing.Size(114, 17);
            this.theLabel.TabIndex = 1;
            this.theLabel.Text = "NOT SELECTED";
            this.theLabel.Click += new System.EventHandler(this.theLabel_Click);
            // 
            // MyComposedControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.theLabel);
            this.Controls.Add(this.theCheckbox);
            this.Name = "MyComposedControl";
            this.Size = new System.Drawing.Size(314, 97);
            this.Load += new System.EventHandler(this.MyComposedControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox theCheckbox;
        private System.Windows.Forms.Label theLabel;
    }
}
