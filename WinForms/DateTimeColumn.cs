﻿using System;
using System.Windows.Forms;

namespace WinForms
{
    public class DateTimeColumn : DataGridViewColumn
    {
        public DateTimeColumn() : base(new DateTimeCell())
        {
        }

        public override DataGridViewCell CellTemplate
        {
            get
            {
                return base.CellTemplate;
            }
            set
            {
                // Ensure that the cell used for the template is a CalendarCell. 
                if (value != null &&
                    !value.GetType().IsAssignableFrom(typeof(DateTimeCell)))
                {
                    throw new InvalidCastException("Must be a DateCell");
                }
                base.CellTemplate = value;
            }
        }
    }

}
