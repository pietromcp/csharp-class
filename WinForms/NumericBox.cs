﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace WinForms
{
    public class NumericBox : TextBox
    {
        [Category("WaltDisney")]
        public Double Value
        {
            get { return Convert.ToInt32(Text); }
            set { Text = value.ToString(); }
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
        }
    }
}
