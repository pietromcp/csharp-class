﻿using System.Windows.Forms;
using System.ComponentModel;

namespace WinForms
{
    public partial class MyComposedControl : UserControl
    {
        public MyComposedControl()
        {
            InitializeComponent();
            InitializeHandlers();
        }

        private void InitializeHandlers() {
            theCheckbox.CheckStateChanged += (source, args) =>
            {
                string newText = theCheckbox.CheckState == CheckState.Checked ? "SELECTED" : "NOT SELECTED";
                this.Invoke((MethodInvoker) (() => {
                    theLabel.Text = newText;
                }));
            };
        }

        [Category("WaltDisney")]
        public string Message
        {
            get { return theCheckbox.Text; }
            set { theCheckbox.Text = value; }
        }

        private void theLabel_Click(object sender, System.EventArgs e)
        {

        }

        private void MyComposedControl_Load(object sender, System.EventArgs e)
        {

        }
    }
}
