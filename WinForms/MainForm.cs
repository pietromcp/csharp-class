﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using WinForms.People;

namespace WinForms
{
    public partial class MainForm : Form
    {
        private bool running = true;
        private BindingSource bindingSource = new BindingSource();
        public MainForm()
        {
            InitializeComponent();
            InitializeWorkers();
            InitializeHandlers();
            InitTableData();
        }

        private void InitializeHandlers()
        {
            this.result.HandleDestroyed += (src, args) => StopCalculation();
            this.dataGridView1.CellValueChanged += (src, args) => CellValueChanged();
            this.dataGridView1.CellParsing += dataGridView1_CellParsing;
            this.myLabel1.OnUpdate += myLabel1_OnUpdate;
            this.myLabel2.OnUpdate += myLabel2_OnUpdate;
        }

        private void dataGridView1_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            if (this.dataGridView1.Columns[e.ColumnIndex].Name == "BirthdayCol")
            {
                if (e != null)
                {
                    if (e.Value != null)
                    {
                        try
                        {
                            // Map what the user typed into UTC.
                            e.Value = Date.From(e.Value.ToString());
                            // Set the ParsingApplied property to 
                            // Show the event is handled.
                            e.ParsingApplied = true;

                        }
                        catch (FormatException)
                        {
                            // Set to false in case another CellParsing handler
                            // wants to try to parse this DataGridViewCellParsingEventArgs instance.
                            e.ParsingApplied = false;
                        }
                    }
                }
            }
        }

        private void CellValueChanged()
        {
            Console.WriteLine($"CurrenItemChanged: {this.bindingSource.Current}");
        }

        private void InitializeWorkers()
        {
            Thread worker = new Thread(() =>
            {
                int x0 = 1;
                int x1 = 1;
                Thread.Sleep(500);
                while (running)
                {
                    int next = x0 + x1;
                    x0 = x1;
                    x1 = next;
                    this.Invoke((MethodInvoker)delegate
                    {
                        result.Text += $" {next}"; // runs on UI thread
                    });
                    Thread.Sleep(500);
                }
            });
            worker.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string username = this.username.Text;
            string password = this.password.Text;
            Console.WriteLine($"{username}:{password}");
            result.Text = $"{username}:{password}";

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void result_TextChanged(object sender, EventArgs e)
        {

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var frm = new OtherForm();
            frm.Location = this.Location;
            frm.StartPosition = FormStartPosition.Manual;
            frm.FormClosing += delegate { this.Show(); };
            frm.Show();
            this.Hide();
        }

        private void MainForm_Closing(object sender, EventArgs e)
        {
            StopCalculation();
        }

        private void StopCalculation()
        {
            running = false;
        }

        private void StartCalculation()
        {
            running = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            StartCalculation();
        }

        private void stop_Click(object sender, EventArgs e)
        {
            StopCalculation();
        }

        private void numericBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void myLabel1_OnUpdate(object sender, EventArgs e)
        {
            this.Invoke((MethodInvoker)delegate ()
            {
                this.myLabel1.Text = Char.IsUpper(this.myLabel1.Text.First()) ? this.myLabel1.Text.ToLowerInvariant() : this.myLabel1.Text.ToUpper();
            });
        }

        private void myLabel2_OnUpdate(object sender, EventArgs e)
        {
            this.Invoke((MethodInvoker)delegate ()
            {
                this.myLabel2.Text = Char.IsUpper(this.myLabel2.Text.First()) ? this.myLabel2.Text.ToLowerInvariant() : this.myLabel2.Text.ToUpper();
            });
        }


        private void InitTableData()
        {

            //bindingSource.Set = false;
            dataGridView1.DataSource = bindingSource;

            foreach (Account acc in Accounts.List)
            {
                bindingSource.Add(acc);
                //int index = this.dataGridView1.Rows.Add();
                //DataGridViewRow row = dataGridView1.Rows[index];
                //row.Cells["UsernameCol"].Value = acc.Username;
                //row.Cells["FirstNameCol"].Value = acc.FirstName;
                //row.Cells["LastNameCol"].Value = acc.LastName;
                //row.Cells["BirthdayCol"].Value = acc.BirthdayAsDate;
                //row.Cells["BirthdayAsDateTimeCol"].Value = acc.BirthdayAsDateTime;
                //row.Cells["IsActiveCol"].Value = acc.IsActive;
            }
        }

        private void save_Click(object sender, EventArgs e)
        {
            IList<Account> accounts = (IList<Account>)bindingSource.List;
            Console.WriteLine($"Save {accounts.Count} accounts");
            foreach (Account acc in accounts)
            {
                Console.WriteLine(acc);
            }
        }
    }
}
