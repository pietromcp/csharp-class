﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace WinForms
{
    public class MyLabel : Control
    {
        private Timer _timer;
        private Int32 _interval = 1000;

        public event EventHandler OnUpdate;

        public Int32 Interval
        {
            get { return _interval; }
            set { _interval = value; }
        }

        public MyLabel()
        {
            _timer = new Timer();
            _timer.Interval = _interval;
            _timer.Tick += new EventHandler(Timer_Tick);
            _timer.Start();

        }

        void Timer_Tick(object sender, EventArgs e)
        {
            if (OnUpdate != null)
            {
                OnUpdate(this, EventArgs.Empty);
                Invalidate();
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.DrawEllipse(new Pen(new SolidBrush(Color.Blue)), new RectangleF(0, 0, 40, 60));
            e.Graphics.DrawString(Text, base.Font, new SolidBrush(Color.Red), 0, 0);
        }

    }
}
