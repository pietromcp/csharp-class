﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsReloaded
{
    class Class1 : Control
    {
        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.DrawEllipse(new Pen(new SolidBrush(Color.Blue)), new RectangleF(0, 0, 40, 60));
            e.Graphics.DrawString(Text, base.Font, new SolidBrush(Color.Red), 0, 0);
        }
    }
}
