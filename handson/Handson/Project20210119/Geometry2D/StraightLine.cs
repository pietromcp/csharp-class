﻿namespace Project20210119.Geometry2D
{
    class StraightLine
    {
        /*
            y = mx + q --> q = y - mx

            ax + by + c = 0
         */
        private double m, q;
        public StraightLine(Point a, Point b) {
            m = (a.Y - b.Y) / (a.X - b.X);
            q = a.Y - m * a.X;
        }

        public bool Contains(Point candidate) {
            return candidate.Y == m * candidate.X + q;
        }
    }
}
