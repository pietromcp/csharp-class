﻿namespace Project20210119.Geometry2D
{
    class Rectangle
    {
        public Rectangle(double width, double height)
        {
            Width = width;
            Height = height;
        }

        public double Width { get; }
        public double Height { get; }

        public double Area => Width * Height;
        public double Perimeter { get { return 2 * (Width + Height); } }
    }
}
