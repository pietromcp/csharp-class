﻿using MyFIrstClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project20210119
{
    

    class Multiplier
    {
        private int factor;

        public Multiplier(int factor) { this.factor = factor; }

        public int Multiply(int x) => x * factor;
    }

    class Program
    {
        static void Foo() { }

        void Bar() { }

        static void Main(string[] args)
        {
            Foo();
            // Bar();
            Program.Foo();
            bool result = string.IsNullOrEmpty("");
            bool result1 = System.String.IsNullOrEmpty("");

            var hello = new Hello();
            //Console.WriteLine("Hello World!");
            Person p = new Person("Pietro", "Martinelli");
            Console.WriteLine($"The full name is {p.GetFullName()}");

            Person em = new Person("Eddy", "Merckx");
            Console.WriteLine($"The full name is {em.GetFullName()}");

            Person fc = new Person("Fausto", "Coppi");
            Console.WriteLine($"The full name is {fc.GetFullName()}");

            Person3.DoSomething();
            Person3.DoSomething();
            Person3.DoSomething();
            Person3.DoSomething();
            //Console.WriteLine($"Full name in main: {p.firstName} {p.lastName}");
            //Point origin = new Point(0, 0);
            //Point a = new Point(-19, 19);
            //StraightLine line = new StraightLine(origin, a);
            //Point candidate0 = new Point(12.5, -12.5);
            //Point candidate1 = new Point(15.5, 17);
            //Console.WriteLine($"Line contains candidate0? {line.Contains(candidate0)} Expected true");
            //Console.WriteLine($"Line contains candidate1? {line.Contains(candidate1)} Expected false");

            //var wallet = new Wallet("Portafoglio di Pietro");
            //wallet.Add(new Stock("Company X", 10.0), 5); // 50
            //wallet.Add(new Stock("Company Y", 5.5), 10); // 55
            //wallet.Add(new Stock("Company Z", 1.1), 100); // 110
            // items = new List<>();
            //Console.WriteLine($"Total wallet value is {wallet.TotalValue} - Expected 215");

            //String s = "Pietro";
            //Counter cnt = new Counter();
            //cnt.Increment();
            //Console.WriteLine($"BEFORE: {cnt.Value}");
            //DoSomething(s, cnt);
            //Console.WriteLine($"OUT: {s}");
            //Console.WriteLine($"AFTER: {cnt.Value}");
            //DoSomething(out int n);
            //Console.WriteLine($"n = {n}");

            //if (int.TryParse("aaa", out int res)) {
            //    Console.WriteLine($"res = {res}");
            //}


            //UseStrings(11); // UseStrings(11, new string[] {})
            //UseStrings(12, "Pietro"); // UseStrings(12, new string[] {"Pietro"})
            //UseStrings(13, "Pietro", "Martinelli");
            //UseStrings(14, "Pietro", "Martinelli", "C#");

            //UseDefault(11, 12, "martinellip"); // UseDefault(11, 12, "martinellip")
            //UseDefault(11, 12); // UseDefault(11, 12, "pietrom")
            //UseDefault(11); // UseDefault(11, 20, "pietrom")
            //UseDefault(11, third: "martinellip", second: 123);

            //SendEmail("Bla bla bla", "pietro@codiceplastico.com", "paolo@codiceplastico.com");
            //SendEmail("Bla bla bla", from: "pietro@codiceplastico.com", to: "paolo@codiceplastico.com");


            //var r1 = new Rectangle(10, 15);           

            //var r2 = new Rectangle(2, 3);

            //Console.WriteLine("R1");
            //Console.WriteLine($"Area: {r1.Area} Expected 150");
            //Console.WriteLine($"Perimetro: {r1.Perimeter} Expected 50");

            //Console.WriteLine("R2");
            //Console.WriteLine($"Area: {r1.Area} Expected 6");
            //Console.WriteLine($"Perimetro: {r1.Perimeter} Expected 10");
        }

        static void UseDefault(int first, int second = 20, string third = "pietrom")
        {
            Console.WriteLine($"first = {first} second = {second} third = {third}");
        }

        static void UseDefaultHandMade(int first, int second, string third) { }

        static void UseDefaultHandMade(int first)
        {
            UseDefaultHandMade(first, 20);
        }

        static void UseDefaultHandMade(int first, int second)
        {
            UseDefaultHandMade(first, second, "pietrom");
        }

        static void UseDefaultHandMade(int first, string third)
        {
            UseDefaultHandMade(first, 20, third);
        }

        static void UseStrings(int n, params string[] values)
        {
            foreach (var v in values)
            {
                Console.WriteLine($"- {v}");
            }
        }



        //static void DoSomething(ref int m) {
        //    m = 11;
        //}

        static void DoSomething(out int m)
        {
            m = 12;
        }

        static void DoSomething(string text, Counter c)
        {
            text = "Martinelli";
            Console.WriteLine($"IN: {text}");
            c.Increment();
            c.Increment();
        }
    }

    class Counter
    {
        public int Value { get; private set; } = 0;

        public void Increment()
        {
            Value++; // Value__Set(Value__Get() + 1)
        }
    }

    class OtherCounter
    {
        public int Value { get; private set; }

        public OtherCounter(int initialValue)
        {
            Value = initialValue;
        }

        public OtherCounter() : this(0)
        {
        }
    }

    class MyMath
    {
        public int Max(int x, int y) // MyMath.Max(int, int)
        {
            return x >= y ? x : y;
        }

        public string Max(string x, string y) // MyMath.Max(string, int)
        {
            return x.CompareTo(y) >= 0 ? x : y;
        }

        public long Max(long x, long y) => x >= y ? x : y;

        public void DoSomething(int x) => Console.WriteLine("");
    }


    class Person
    {
        //private string firstName;
        //private string lastName;
        private string[] fields;
        private int id;

        private static int NextId = 1;

        static Person()
        {
            Console.WriteLine("Before the first Person!");
        }


        public Person(string first, string last)
        {
            Console.WriteLine("Person's constructor");
            id = NextId;
            NextId++;
            fields = new[] { first, last };
            //firstName = first;
            //lastName = last;
        }

        public Person(string last)
        {
            fields = new[] { "Pietro", last };
        }

        //public Person(string last) : this("Pietro", last)
        //{ }

        public string GetFullName()
        {
            return $"[{this.id}] {fields[0]} {fields[1]}";
        }

        public void SetId(int id)
        {
            // id ???
            this.id = id;
        }

        public string FirstName
        {
            get
            {
                return fields[0];
            }

            set
            {
                fields[0] = value;
            }
        }

        public string FullName
        {
            get
            {
                return $"{fields[0]} {fields[1]}";
            }
        }

        public string FullName2 => $"{fields[0]} {fields[1]}";
    }

    class Person2
    {
        private string firstName, lastName;
        public string FullName
        {
            get => $"{firstName} {lastName}";
            set
            {
                string[] fields = value.Split(' ');
                firstName = fields[0]; lastName = fields[1];
            }
        }
    }
    // var p = new Person2(); p.FullName = "Pietro Martinelli";


    class Person3
    {
        private string firstName, lastName;

        static Person3()
        {
            Console.WriteLine("Person3's  static initializer");
        }

        public static void DoSomething()
        {
            Console.WriteLine("Person3.DoSomething()");
        }

        public string GetFullName() => $"{firstName} {lastName}";

        public void FullName(string value)
        {
            string[] fields = value.Split(' ');
            firstName = fields[0]; lastName = fields[1];
        }
    }
    // var p = new Person3(); p.SetFullName("Pietro Martinelli");

}
