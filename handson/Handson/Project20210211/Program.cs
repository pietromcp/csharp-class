﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace Project20210211
{
    class Program
    {
        const string Url = "https://jsonplaceholder.typicode.com/users";

        static void Main(string[] args) // MainSync V2
        {
            DoHttpCallAsync()
                .ContinueWith(fullTask => {
                    Console.WriteLine($"Aggregated task completed [{fullTask.Exception}]");

                });
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
        
        //static void Main(string[] args) // MainSync V1
        //{
        //    //DoHttpCall();
        //    DoHttpCallAsync();
        //    Console.WriteLine("Press any key to exit");
        //    Console.ReadKey();
        //}


        //static async Task Main(string[] args) // MainAsync
        //{
        //    await DoHttpCallAsync();
        //    Console.WriteLine("Press any key to exit");
        //    Console.ReadKey();
        //}

        static void DoHttpCall()
        {
            var client = new HttpClient();
            var fullTask = client.GetAsync(Url)
                .ContinueWith(ReadResponseBody)
                .Unwrap()
                .ContinueWith(DeserializeUsers)
                .Unwrap()
                .ContinueWith(LogUsers);
            fullTask.Wait();
            Console.WriteLine($"Full task completed: {fullTask.Result.Count()} users found");
        }

        static async Task DoHttpCallAsync() {
            var client = new HttpClient();
            var response = await client.GetAsync(Url);
            var stream = await response.Content.ReadAsStreamAsync();
            var items = await JsonSerializer.DeserializeAsync<UserData[]>(stream);
            Console.WriteLine($"Full task completed: {items.Count()} users found");
        }

        private static Task<System.IO.Stream> ReadResponseBody(Task<HttpResponseMessage> t0)
        {
            var response = t0.Result;
            Console.WriteLine($"Status code is {response.StatusCode}");
            var content = response.Content;
            return content.ReadAsStreamAsync();
        }

        private static Task<UserData[]> DeserializeUsers(Task<Stream> t1) {
            var responseBodyStream = t1.Result;
            return JsonSerializer.DeserializeAsync<UserData[]>(responseBodyStream).AsTask();
        }

        private static UserData[] LogUsers(Task<UserData[]> t2) {
            var items = t2.Result;
            Console.WriteLine($"Got {items.Count()} users");
            foreach (UserData item in items)
            {
                Console.WriteLine($"Name: {item.name} Username: {item.username} Email: {item.email}");
            }
            return items;
        }
    }

    class UserData { 
        public long Id { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string email { get; set; }
    }
}
