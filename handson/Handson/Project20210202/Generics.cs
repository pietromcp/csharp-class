﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project20210202
{
    static class Generics
    {
        public static void UseGenerics() {
            IList<int> myInts = new[] { 1, 2, 3 };

            IDictionary<int, string> statuses = new Dictionary<int, string>();
            statuses.Add(200, "Ok");
            statuses.Add(404, "Not Found");
            statuses.Add(418, "I'm a teapot");

            var notFound = statuses[404];
            var containsServerError = statuses.ContainsKey(500);

            IDictionary<string, int> ages = new Dictionary<string, int>() {
                { "Pietro Martinelli", 42 },
                { "Eddy Merckx", 76 },
                { "Alberto Contador", 38 }
            };

            var pm = ages["Pietro Martinelli"];

            var mappedStatuses = new MyMap<int, string>();
            mappedStatuses.Add(200, "Ok");
            mappedStatuses.Add(404, "Not Found");
            mappedStatuses.Add(418, "I'm a teapot");
            Console.WriteLine($"418 --> {mappedStatuses[418]}");

            Mapper<string, int> stringToIntMapper = new MyStringToIntMapper();
            UseMapper(stringToIntMapper);
            UseMapperDelegate(txt => txt.Length + 10);
            UseMapperDelegate(stringToIntMapper.Map);
            //UseMapperAsMapperDelegate();
            MapperDelegate<string, string> f1 = (text) => text.Replace("t", "");
            MapperDelegate<string, int> f2 = (text) => text.Length;
            var composed = Composed(f1, f2);
            Console.WriteLine($"Invoke Composed {composed("Pietro Martinelli")}");

            IList<string> myStrings = new[] { "aa", "bb" };
            //IList<object> myObjects = myStrings; // Compilation error
            //myObjects.Add(1919);

            MyList<string> my2 = null;
            MyList<object> myObj = my2;

            MyConsumer<object> myObjCons = null;
            MyConsumer<string> myStringCons = myObjCons;

        }

        delegate int FuncInt(int a);

        static MapperDelegate<A, C> Composed<A, B, C>(MapperDelegate<A, B> f, MapperDelegate<B, C> g) {
            return (A a) => g(f(a));
        }

        static void UseMapper(Mapper<string, int> mapper) {
            Console.WriteLine($"Mapping...{mapper.Map("pietrom")}");
        }

        static void UseMapperDelegate(MapperDelegate<string, int> mapper)
        {
            Console.WriteLine($"Mapping...{mapper("martinellip")}");
        }

        static void UseGenericMapperDelegate<A, B>(MapperDelegate<A, B> mapper)
        {
            Console.WriteLine($"Mapping...{mapper(default(A))}");
        }

        static void UseMapperAsMapperDelegate<X, Y>(Mapper<X, Y> mapper) {
            UseGenericMapperDelegate<X, Y>(mapper.Map);
        }
    }

    interface MyList<out T> {
        T GetAt(int index);

        //void Add(T newValue); // Compilation error
    }

    interface MyConsumer<in T> {
        //T GetAt(); // Compilation error

        void Consume(T value);
    }



    class MyStringToIntMapper : StringMapper<int>
    {
        public int Map(string input) => input.Length;
    }

    class MyMap<TKey, TVal> {
        private readonly IList<MyMapItem<TKey, TVal>> items = new List<MyMapItem<TKey, TVal>>();

        public void Add(TKey k, TVal v) {
            items.Add(new MyMapItem<TKey, TVal> { Key = k, Value = v });
        }

        public TVal this[TKey key] {
            get {
                foreach (var item in items) {
                    if (item.Key.Equals(key)) {
                        return item.Value;
                    }
                }
                return default(TVal);
            }
        }
    }

    interface Mapper<TIn, TOut> {
        TOut Map(TIn input);
    }

    delegate TOut MapperDelegate<TIn, TOut>(TIn input);

    interface StringMapper<TOut> : Mapper<string, TOut> { }

    class MyMapItem<A, B> {
        public A Key { get; set; }
        public B Value{ get; set; }
    }

    interface Repository<TId, TEntity> where TEntity: class, Entity<TId>, new() {
        TId Insert(TEntity entity);
        TEntity Get(TId id);
        IList<TEntity> GetAll();
    }

    class WrongRepo : Repository<PersonId, Person>
    {
        public Person Get(PersonId id)
        {
            throw new NotImplementedException();
        }

        public IList<Person> GetAll()
        {
            throw new NotImplementedException();
        }

        public PersonId Insert(Person entity)
        {
            throw new NotImplementedException();
        }
    }

    interface Entity<TKey> { 
        public TKey Id { get; }
    }

    record PersonId { 
        public string Value { get; init; }
    }

    class Person : Entity<PersonId>
    {
        public PersonId Id { get; init; }
    }
}
