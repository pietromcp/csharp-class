﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Project20210202
{
    class Program
    {
        static void Main(string[] args)
        {
            var p = new Parent();
            Child c = new Child();
            p.Foo();
            c.Foo();
            p.Bar();
            c.Bar();
            Parent p1 = new Parent();
            Parent c1 = new Child();
            Console.WriteLine("Foo");
            p1.Foo();
            c1.Foo();
            Console.WriteLine("FooBar");
            p1.Bar();
            c1.Bar();
            Console.WriteLine("FooBar");
            p1.FooBar();
            c1.FooBar();
            Console.WriteLine("---- IS");
            Console.WriteLine($"Parent is Parent? {p1 is Parent}");
            Console.WriteLine($"Parent is Child? {p1 is Child}");
            Console.WriteLine($"Child is Parent? {c1 is Parent}");
            Console.WriteLine($"Child is Child? {c1 is Child}");
            Console.WriteLine("---- AS");
            Console.WriteLine($"Parent as Parent {p1 as Parent}");
            var cantConvert = p1 as Child;
            Console.WriteLine($"Parent as Child {p1 as Child}");
            Console.WriteLine($"Is null? {cantConvert == null}");
            Console.WriteLine($"Child as Parent {c1 as Parent}");
            Console.WriteLine($"Child as Child {c1 as Child}");
            
            //Console.WriteLine($"Cast Parent as Parent {(Parent)p1}");
            //Console.WriteLine($"Cast Parent as Child {(Child)p1}");
            //Console.WriteLine($"Cast Child as Parent {(Parent)c1}");
            //Console.WriteLine($"Cast Child as Child {(Child)c1}");
            //Console.WriteLine($"Cast Parent as Child {(Parent)c}");

            Calculator mc = new MyVeryCoolCalculator();
            var asList = mc as List<int>;


            Calculator calc = new MyVeryCoolCalculator();
            Console.WriteLine($"Calc: {calc.Calculate("aaa")}");
            Console.WriteLine($"Calc: {calc.Exec("aaa")}");
            Intepreter inter = new MyVeryCoolCalculator();
            Console.WriteLine($"Inter: {inter.Calculate("aaa")}");
            Console.WriteLine($"Inter: {inter.Exec("aaa")}");
            MyVeryCoolCalculator coolCalculator = new MyVeryCoolCalculator();
            Console.WriteLine($"coolCalculator: {coolCalculator.Calculate("aaa")}");
            Console.WriteLine($"coolCalculator: {coolCalculator.Exec("aaa")}");


            var bar = new MyRectangle();
            //var cccc = bar as Calc0; // COmpilation error: MyRectangle is sealed

            Generics.UseGenerics();

        }
    }

    interface Calc0 {
        int Calculate(string input);
    }

    interface Calc1 {
        int Calculate(string input);
    }

    class StupidCalculator : Calc0, Calc1 {
        int Calc0.Calculate(string input) => input.Length;
        int Calc1.Calculate(string input) => input.Length - 10;
    }

    interface Calculator {
        int Calculate(string input);

        int Exec(string input);
    }

    interface Intepreter
    {
        int Calculate(string text);

        double Exec(string text);
    }

    class MyVeryCoolCalculator : Calculator, Intepreter {
        public int Calculate(string anInput) { return 19; }

        int Calculator.Exec(string anInput) { return 17; }

        double Intepreter.Exec(string text)
        {
            return 19.19;
        }

        public string Exec(string text) => text.ToUpper();

        //public double Exec(string anInput) { return 17; }
    }


    abstract class Level0 {
        public abstract void DoSomething();
    }

    class Level1 : Level0
    {
        public override void DoSomething()
        {
            Console.WriteLine("DoSomething Level1");
        }
    }
    class Level2 : Level1
    {
        public override void DoSomething()
        {
            Console.WriteLine("DoSomething Level2");
        }
    }

    abstract class Level3 : Level2 {
        public override abstract void DoSomething();
    }

    class Level4 : Level3
    {
        public override void DoSomething() {
            Console.WriteLine("DoSomething Level4");
        }
    }



    interface OverTheTop {
        void FooFooFoo();
    }

    abstract class Top : OverTheTop
    {
        public abstract void BarBarBar();
        public abstract void FooFooFoo(); // In Java questo è inutile
    }

    abstract class GrandParent : Top {
        public abstract void FooBar();        
    }

    class Parent : GrandParent {
        public override void BarBarBar()
        {
            throw new NotImplementedException();
        }

        public override void FooFooFoo()
        {
            throw new NotImplementedException();
        }

        public void Foo() {
            Console.WriteLine("Foo in Parent");
        }

        public virtual void Bar()
        {
            Console.WriteLine("Bar in Parent");
        }

        public override void FooBar()
        {
            Console.WriteLine("FooBar in Parent");
        }
    }

    class Child : Parent {
        public new void Foo() {
            Console.WriteLine("Foo in Child");
        }
        public new void Bar()
        {
            Console.WriteLine("Bar in Child");
        }

        public override void FooBar()
        {
            Console.WriteLine("FooBar in Child");
        }
    }

    class NextChild : Child { 
    }

    class NextNextChild : NextChild
    {
    }

    abstract class Shape
    {
        public abstract double Area();

        public abstract double Perimeter { get; }

        public string AsString()
        {
            return $"Area: {Area()}, Perimeter: {Perimeter}";
        }
    }

    interface MyShape
    {
        double Area(); // public abstract

        public abstract double Perimeter { get; }

        public string AsString()
        {
            return $"Area: {Area()}, Perimeter: {Perimeter}";
        }
    }

    interface IFoo {
        void Foo();
    }

    interface IBar
    {
        int Bar(string text);
    }

    class MyFakeSuperclass { }

    // class MyRectangle extends MyFakeSuperclass implements IBar, IFoo, IDisposable
    sealed class MyRectangle : MyFakeSuperclass, IBar, IFoo, IDisposable
    {
        public double Perimeter => 10;

        public double Area()
        {
            return 20;
        }

        public int Bar(string text)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void Foo()
        {
            throw new NotImplementedException();
        }
    }

    class Square : Shape
    {
        public override double Perimeter => 100;

        public override double Area()
        {
            return 200;
        }

        /*
         public double Perimeter => 10;

        public double Area()
        {
            return 20;
        }
         */
    }

}
