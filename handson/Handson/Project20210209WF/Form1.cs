﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Timer = System.Windows.Forms.Timer;
using TimerOld = System.Timers.Timer;

namespace Project20210209WF
{
    public partial class Form1 : Form
    {
        private static readonly string[] Messages = new[] { "", "Hello, World!", "Pietro Martinelli", "Windows Forms", "Hello, WinForms!" };
        private Timer timer;
        private TimerOld timerOld;
        
        public Form1()
        {
            InitializeComponent();
            CustomizeComponent();
            timer = new Timer() {
                Interval = 500,
                Enabled = true
            };
            timer.Tick += TimerTick;

            timerOld = new TimerOld
            {
                Interval = 500,
                AutoReset = true,
                Enabled = true
            };
            timerOld.Elapsed += TimeOldTick;
        }

        private void TimeOldTick(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.Invoke((MethodInvoker)(() => {
                currentDateAndTime2.Text = DateTimeOffset.Now.ToString();
            }));            
        }

        private void TimerTick(object sender, EventArgs e)
        {
            currentDateAndTime.Text = DateTimeOffset.Now.ToString();
        }

        private void CustomizeComponent()
        {
            this.Text = "Ho cambiato il titolo";
            this.clickMe.Text = "Clicca qui!";
            this.message.Text = "";
        }

        private void clickMe_Click(object sender, EventArgs e)
        {
            var newMessage = Messages.Random();
            this.message.Text = $"{newMessage} - {birthDay.DateValue.AsNormalizedDate()} - {myComposedControl2.DateValue.AsNormalizedDate()}";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = $"Hello, {person.Text}!";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void myCrazyLabel1_Click(object sender, EventArgs e)
        {

        }
    }

    static class ListExtension {
        private static readonly Random Rnd = new Random();

        public static T Random<T>(this IList<T> self) => self == null || self.Count == 0 ? default(T) : self[Rnd.Next(self.Count)];
    }
}
