﻿using System;
using System.Windows.Forms;

namespace Project20210209WF
{
    public partial class MyComposedControl : UserControl
    {
        public MyComposedControl()
        {
            InitializeComponent();
        }        

        public MyDateInfo DateValue {
            get {
                return new MyDateInfo
                {
                    Year = ValueOf(year.Text),
                    Month = ValueOf(month.Text),
                    Day = ValueOf(day.Text)
                };
            }
            set {
                year.Text = value.Year.ToString();
                month.Text = value.Month.ToString();
                day.Text = value.Day.ToString();
            }
        }

        private static int ValueOf(string text) {
            return string.IsNullOrEmpty(text) ? 1 : int.Parse(text);
        }
    }

    public class MyDateInfo { 
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }

        public string AsNormalizedDate()
        {
            return $"{Year}{Month}{Day}";
        }
    }
}
