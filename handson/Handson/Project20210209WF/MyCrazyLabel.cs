﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Project20210209WF
{
    public class MyCrazyLabel : Control
    {
        private static readonly Random Rnd = new Random();

        private Timer timer;

        public int Interval { get; set; } = 500;

        public MyCrazyLabel() {
            timer = new Timer
            {
                Interval = Interval,
                Enabled = true
            };
            timer.Tick += TimerTick;
        }

        private void TimerTick(object sender, EventArgs e)
        {
            Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            var toLeft = Rnd.Next(2) == 0;
            var toTop = Rnd.Next(2) == 0;
            var left = toLeft ? 0 : Width - 50;
            var top = toTop ? 0 : Height - 80;
            e.Graphics.DrawEllipse(new Pen(new SolidBrush(Color.Blue)), new RectangleF(left, top, 50, 80));
        }
    }
}
