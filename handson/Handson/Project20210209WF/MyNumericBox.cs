﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Project20210209WF
{
    public class MyNumericBox : TextBox
    {
        public MyNumericBox() {
            Text = "19";
            ForeColor = System.Drawing.Color.DarkRed;
        }

        [Category("MyCustomProps")]
        public int Value
        {
            set
            {
                Text = value.ToString();
            }

            get
            {
                return int.Parse(Text);
            }
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar)) {
                e.Handled = true;
            }
            base.OnKeyPress(e);
        }
    }
}
