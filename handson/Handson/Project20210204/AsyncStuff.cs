﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project20210204
{
    class AsyncStuff
    {
        event MyEventHandler OnEvent;

        async Task DoSomething() {
            await OnEvent.Invoke();
        }
    }

    delegate Task MyEventHandler();


}
