﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project20210204
{
    class TaskSample
    {
        private static readonly string[] names = new[] { "AA", "BB", "CC", "DD", "EE" };

        internal static void RunWithAsyncAwait() {
            RunAsync().ContinueWith(testc => Console.WriteLine("RunWithAsyncAwait completed"));
            Thread.Sleep(2000);

            DoSomething2Async(100).ContinueWith(t0 => {
                DoSomething2Async(200).ContinueWith(t1 => {
                    DoSomething2Async(300).ContinueWith(t2 => {
                        DoSomething2Async(400).ContinueWith(t3 => {
                            //if(t0.Exception == null && t1.Exception == null && ...)
                            // t0.Result + t1.Result + .....
                        });
                    });
                });
            });
        }
        
        static async Task RunAsync() {
            var tasks = names.Select(n => Task.Run(() => n.ToLowerInvariant())).ToArray();
            var results = await Task.WhenAll(tasks);
            Console.WriteLine($"Result {string.Join('|', results)}");

            try
            {
                var r0 = await DoSomething2Async(100);
                var r1 = await DoSomething2Async(200);
                var r2 = await DoSomething2Async(300);
                var r3 = await DoSomething2Async(400);
                // r0 + r1 + ...
            }
            catch (Exception e) { 

            }
        }

        static async Task<string> GetStringAsync() {
            await Task.Delay(100);
            return "pietrom";
        }

        internal static void Run()
        {
            

            //Task t0 = Task.Run(CreateTask("XX"));
            //Task t1 = new Task(CreateTask("YY"));
            //t1.Start();

            //Console.WriteLine("Main thread");
            
            var tasks = names.Select(name => Task.Run(CreateTask(name))).ToList();
            var join = Task.WhenAll(tasks);
            var first = Task.WhenAny(tasks);

            join.ContinueWith(t => {
                Console.WriteLine("All tasks completed");
            });

            first.ContinueWith(t => {
                Console.WriteLine("First task completed");
            });

            DoSomethingAsync(300).ContinueWith(t => {
                Console.WriteLine("Task n completed");
            });

            // Task
            Task.CompletedTask.ContinueWith(t => Console.WriteLine("Already completed"));

            // Task<int>
            Task.FromResult(12345).ContinueWith(t => Console.WriteLine($"Completed with result {t.Result}"));

            var lowerCaseTasks = names.Select(n => Task.Run(() => n.ToLowerInvariant())).ToArray();
            var lowerJoin = Task.WhenAll(lowerCaseTasks);
            var lowerAny = Task.WhenAny(lowerCaseTasks).Unwrap();
            
            lowerJoin.ContinueWith(t => Console.WriteLine(string.Join(',', t.Result)));
            lowerAny.ContinueWith(t => Console.WriteLine($"First task returned {t.Result}"));

            Thread.Sleep(3000);
        }

        static Random Random = new Random();        

        static Action CreateTask(string taskName) {
            return () =>
            {
                Console.WriteLine($"Starting task {taskName}");
                Thread.Sleep(Random.Next(400));
                Console.WriteLine($"Completed task {taskName}");
            };
        }

        static Task DoSomethingAsync(int n) {
            return Task.Run(() => {
                Thread.Sleep(n);
                Console.WriteLine($"After {n} millis...");
            });
        }

        static Task<int> DoSomething2Async(int n)
        {
            return Task.FromResult(n);
        }
    }
}
