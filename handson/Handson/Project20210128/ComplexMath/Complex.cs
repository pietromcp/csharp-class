namespace Project20210128.ComplexMath {
    class Complex
    {
        // (x, y) --> x + iy
        // (a, b) + (c, d) = (a + b, c + d)
        public Complex(int re, int im)
        {
            Re = re;
            Im = im;
        }

        public int Re { get; }
        public int Im { get; }

        public override string ToString()
        {
            return $"({Re}, {Im})";
        }

        // var sum = c0.Add(c1);
        public Complex Add(Complex that) => new Complex(this.Re + that.Re, this.Im + that.Im);

        // var sum = Complex.Add(c0, c1);
        public static Complex Add(Complex x, Complex y) => new Complex(x.Re + y.Re, x.Im + y.Im);
        // public static Complex Add(Complex x, Complex y) => x.Add(y);

        // var sum = c0 + c1;
        public static Complex operator +(Complex x, Complex y)
        {
            return new Complex(x.Re + y.Re, x.Im + y.Im);
        }

        public static implicit operator Complex(int n) {
            return new Complex(n, 0);
        }

        public static explicit operator int(Complex c)
        {
            return c.Re;
        }

        public static Complex operator -(Complex x)
        {
            return new Complex(-x.Re, -x.Im);
        }

        public static Complex operator *(Complex x, int that) {
            return new Complex(x.Re * that, x.Im * that);
        }
    }
}