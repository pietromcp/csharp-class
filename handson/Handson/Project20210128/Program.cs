﻿using System;
using System.Collections.Generic;
using System.Timers;
using Project20210128.ComplexMath;

namespace Project20210128 {
    class Program {
        static void Main(string[] args) {
            Inheritance.UseInheritance();
            UseOperators();
            UseEvents();

            var obj = new ABrandNewClass(11, 19);
            var sum = obj.Sum;
            var arrowSum = obj.ArrowSum;

            var product = obj.CalculateProduct(17);
            var arrowProduct = obj.ArrowCalculateProduct(17);

            Delegates();
        }
        
        static void Delegates() {
            var input = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var output = MySelect(input, x => x * 2);
            //var output = MySelect(input, (int x) => x * 2);
            //var output = MySelect(input, delegate(int x) { return x * 3; });

            //var output = MySelect(input, new Multiplier(5).Multiply);

            //MyIntFunction myLocalFunc = (int y) => y * 7;
            //var output = MySelect(input, myLocalFunc);
            //Console.WriteLine(string.Join(',', output));

            UseMyVeryCoolDelegate(delegate(int p0, int p1, string p2, double p3) { return p3 * (p0 + p1 + p2.Length ); });
            UseMyVeryCoolDelegate((int p0, int p1, string p2, double p3) => p3 * (p0 + p1 + p2.Length));
            UseMyVeryCoolDelegate((p0, p1, p2, p3) => p3 * (p0 + p1 + p2.Length));
            UseMyVeryCoolDelegate((p0, p1, p2, p3) => { 

                return p3 * (p0 + p1 + p2.Length); 
            });
            var calculator0 = new MyVeryCoolClass(10);
            UseMyVeryCoolDelegate(calculator0.Calculate);
            var calculator1 = new MyVeryCoolClass(100);
            UseMyVeryCoolDelegate(calculator1.Calculate);
        }
        
        static void UseEvents() {
            //var source = new MyEventSource();
            //source.EventOccurred += (num, str) => {
            //    Console.WriteLine($"Handler 0 {num} {str}");
            //};
            //source.EventOccurred += (n, s) => {
            //    Console.WriteLine($"Handler 1 {n} {s}");
            //};
            //source.EventOccurred += (n, s) => {
            //    Console.WriteLine($"Handler 2 {n} {s}");
            //};
            //source.DoSomethingAndNotifyHandlers(11, 19, 17, "Hello, pietrom!");

            var timer = new Timer {
                Interval = 1000
            };

            var timer1 = new Timer(interval: 1000);


            timer.Elapsed += (sender, args) => {
                Console.WriteLine($"Timer invoked at {args.SignalTime}");
            };
            timer.Start();
            Console.WriteLine("Press any key to stop");
            Console.ReadKey();
            timer.Stop();
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        static void UseOperators() {
            var c0 = new Complex(11, 19);
            var c1 = new Complex(17, 22);
            var sum = c0 + c1;
            Console.WriteLine($"sum is {sum}");
            var scaled = c1 * 10;
            Console.WriteLine($"scaled is {scaled}");            
            var negate = -c0;
            Console.WriteLine($"negate is {negate}");
            var sumWithInt = c0 + 100; // (a + ib) + c = (a + c) + ib
            var sumWithIntSwitched = 200 + c0;
            Console.WriteLine($"sumWithInt is {sumWithInt}");
            Console.WriteLine($"sumWithIntSwitched is {sumWithIntSwitched}");
            Console.WriteLine($"sqrt is {Math.Sqrt((int)new Complex(25, 15))}");
        }
        
        delegate double MyVeryCoolDelegate(int x, int y, string text, double noise);

        static void UseMyVeryCoolDelegate(MyVeryCoolDelegate function)
        {
            var a = 11;
            var b = 19;
            var n = 0.17d;
            var txt = "Hello, World!";
            var result = function(a, b, txt, n);
            Console.WriteLine($"The result is {result}");
        }
        
        static List<int> MySelect(List<int> ints, MyIntFunction mapper) {
            var result = new List<int>();
            foreach (int i in ints) {
                result.Add(mapper(i));
                //var res = mapper?.Invoke(i);
                //result.Add(mapper?.Invoke(i));
            }
            return result;
        }

        delegate int MyIntFunction(int x); // int => int
    }
    
    class ABrandNewClass { 
        public int X { get; private set; }
        public int Y { get; }

        public ABrandNewClass(int x, int y)
        {
            X = x;
            Y = y;
        }        

        public int Sum {
            get {
                return X + Y;
            }
        }

        public int ArrowSum => X + Y;

        public int CalculateProduct(int noise) {
            return X * Y + noise;
        }

        public int ArrowCalculateProduct(int factor) => X * Y * factor;

        public void DoSomething()
        {
            X = 10;
        }
    }

    class MyVeryCoolClass {
        private readonly int aValue;

        public MyVeryCoolClass(int aValue)
        {
            this.aValue = aValue;
        }

        public double Calculate(int aaa, int bbb, string ccc, double ddd) {
            return ddd + (aaa + bbb) * ccc.Length + aValue;
        }
    }

    delegate void MyEventMarker(int x, string txt);

    class MyEventSource
    {
        public event MyEventMarker EventOccurred;

        public void DoSomethingAndNotifyHandlers(int a, int b, int c, string text) 
        {
            var sum = a + b + b;
            var upper = text.ToUpperInvariant();
            EventOccurred.Invoke(sum, upper);
        }
    }

    partial class MyPartialClass { 
        public int X { get; set; }
    }


}