﻿using System;
using System.Linq;

namespace Project20210121 {
    class Program {
        static void Main(string[] args) {
            Indexers();
            ExtensionMethods();
            NullReference();

            HttpStatusCode ok = HttpStatusCode.Ok;
            PrintHttpStatusCode(ok);
            PrintHttpStatusCode(HttpStatusCode.IAmATeapot);
            PrintHttpStatusCode((HttpStatusCode)123);

            PrintHttpStatusCode(TypeSafeHttpStatusCode.Ok);
            PrintHttpStatusCode(TypeSafeHttpStatusCode.IAmATeapot);
            PrintHttpStatusCode(TypeSafeHttpStatusCode.NotFound);
            //PrintHttpStatusCode(new TypeSafeHttpStatusCode("Boh", 123)); // Compilation error
            //PrintHttpStatusCode((TypeSafeHttpStatusCode)123); // Compilation error
        }

        static void Indexers()
        {
            var myObject = new MyClass();
            myObject[19] = "pietro";
            Console.WriteLine($"Accessing through indexer property: {myObject[19]}");
            Console.WriteLine($"Accessing through indexer property: {myObject[20]}");

            myObject.SetByKey(19, "martinelli");
            Console.WriteLine($"Accessing through method: {myObject.GetByKey(19)}");
            Console.WriteLine($"Accessing through method: {myObject.GetByKey(20)}");


            myObject[19, 11] = "pietro";
            Console.WriteLine($"Accessing through indexer property: {myObject[19, 11]}");
            Console.WriteLine($"Accessing through indexer property: {myObject[20, 12]}");
            Console.WriteLine($"Accessing through indexer property: {myObject[17, 13]}");
        }

        static void ExtensionMethods()
        {
            bool expectedTrue0 = string.IsNullOrEmpty(null);
            bool expectedTrue1 = string.IsNullOrEmpty("");
            bool expectedFalse = string.IsNullOrEmpty("pietrom");

            string text = "something";
            if (!string.IsNullOrEmpty(text))
            {
                Console.WriteLine($"Has value {text}");
            }

            if (text.HasValue())
            { // !string.IsNullOrEmpty(text)
                Console.WriteLine($"Has value 2 {text}");
            }

            if (MyExtensionsContainer.HasValue(text))
            { // !string.IsNullOrEmpty(text)
                Console.WriteLine($"Has value 2 {text}");
            }

            string plate = "AB 123 CD";
            if (plate.IsItalianPlate())
            { // SomeClass.IsItalianPlate(plate)

            }

            var myList = new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
            var results = myList
                .Select(x => x * 3)
                .Where(x => x % 2 == 0)
                .Select(x => x * x);

            var results1 = Enumerable.Select(Enumerable.Where(Enumerable.Select(myList, x => x * 3), x => x % 2 == 0), x => x * x);

            var multiplied = Enumerable.Select(myList, x => x * 3);
            var filtered = Enumerable.Where(multiplied, x => x % 2 == 0);
            var results2 = Enumerable.Select(filtered, x => x * x);

            Console.WriteLine($"Random number from myList: {myList.Random()}");
            Console.WriteLine($"Random number from myList: {myList.Random()}");
            Console.WriteLine($"Random number from myList: {myList.Random()}");
            Console.WriteLine($"Random number from myList: {myList.Random()}");
        }

        static string NullReturningMethod() => null;

        static void NullReference()
        {
            var text0 = "pietrom";
            Console.WriteLine($"{text0} to uppercase is {text0.ToUpper()}");

            string text1 = NullReturningMethod().OrEmpty(); // string text1 = StringNormalizer.GetValueOrEmptyy(NullReturnigMethod())

            //if (text1 != null) {
            //    Console.WriteLine($"{text1} to uppercase is {text1.ToUpper()}");
            //}

            //if (text1 == null) {
            //    text1 = string.Empty;
            //}
            Console.WriteLine($"{text1} to uppercase is {text1.ToUpper()}");

            var text2 = NullReturningMethod().Or("pietro");
            Console.WriteLine($"{text2} to uppercase is {text2.ToUpper()}");
        }

        static void PrintHttpStatusCode(HttpStatusCode code)
        {
            Console.WriteLine($"Code: {code}");
        }

        static void PrintHttpStatusCode(TypeSafeHttpStatusCode code)
        {
            Console.WriteLine($"Code: {code}");
        }
    }

    public enum MyEnum { Ok, Ko, DontKnow }
    public enum HttpStatusCode
    {
        NotFound = 404, Ok = 200, Conflict = 409, IAmATeapot = 418,
        Created = 201, InternalServerError = 500, Forbidden = 403
    }

    public class TypeSafeHttpStatusCode
    {
        private string Text { get; }
        private int Code { get; }

        private TypeSafeHttpStatusCode(string text, int code)
        {
            Text = text;
            Code = code;
        }

        public override string ToString()
        {
            return $"[{Code}] {Text}";
        }

        public static TypeSafeHttpStatusCode Ok = new TypeSafeHttpStatusCode("Ok", 200);
        public static TypeSafeHttpStatusCode NotFound = new TypeSafeHttpStatusCode("NotFound", 404);
        public static TypeSafeHttpStatusCode IAmATeapot = new TypeSafeHttpStatusCode("IAmATeapot", 418);
    }

    static class MyExtensionsContainer
    {
        public static bool HasValue(this string self)
        {
            return !string.IsNullOrEmpty(self);
        }

        public static bool IsItalianPlate(this string self)
        {
            return true;
        }
    }

    static class StringExtension
    {
        public static string Or(this string self, string defaultValue) => self != null ? self : defaultValue;
        public static string Or2(this string self, string defaultValue) => self ?? defaultValue; // var != null ? var : expr    var ?: expr
        public static string OrEmpty(this string self) => self.Or(string.Empty);
    }

    static class ObjectExtension
    {
        public static T Or<T>(this T self, T defayltValue) => self != null ? self : defayltValue;
    }

    static class IntArrayExtension
    {
        private static readonly Random Randomizer = new Random();

        public static int Random(this int[] ints)
        {
            var index = Randomizer.Next(ints.Length);
            return ints[index];
        }
    }

    class MyClass
    {
        private string[] values = new string[1000];

        public string this[int key]
        {
            get
            {
                return values[key];
            }

            set
            {
                values[key] = value;
            }
        }

        public string this[int key0, int key1]
        {
            get
            {
                return values[key0 + key1];
            }

            set
            {
                values[key0 + key1] = value;
            }
        }

        public void SetByKey(int key, string value)
        {
            values[key] = value;
        }

        public string GetByKey(int key)
        {
            return values[key];
        }

        public void SetByKeys(int key1, int key2, string value)
        {
            values[key1 + key2] = value;
        }

        public string GetByKeys(int key1, int key2)
        {
            return values[key1 + key2];
        }
    }
}