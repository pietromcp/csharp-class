﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonRegistry
{
    class Program
    {
        static void Main(string[] args)
        {
            IList<Polygon> polygons = new List<Polygon>();
            polygons.Add(new Rectangle(10, 20));
            polygons.Add(new Square(10));
            polygons.Add(new Hexagon(8));
            polygons.Add(new Pentagon(8));

            foreach (Polygon p in polygons)
            {
                Console.WriteLine($"A: {p.Area}");
                Console.WriteLine($"2p: {p.Perimeter}");
            }
            Console.ReadKey();
        }
    }

    interface Polygon
    {
        double Area { get; }
        double Perimeter { get; }
    }

    class Rectangle : Polygon
    {
        private double w, h;

        public Rectangle(double width, double height)
        {
            w = width;
            h = height;
        }

        public double Area => w * h;

        public double Perimeter => (w + h) * 2;
    }

    class Square : RegularPolygon
    {
        public Square(double edge) : base(edge) { }

        public override int EdgeCount => 4;
    }

    abstract class RegularPolygon : Polygon {
        private double edge;

        public RegularPolygon(double edge)
        {
            this.edge = edge;
        }

        public double Apothema { get {
                return (edge / 2) / Math.Tan(Math.PI / EdgeCount);
            } } 

        public double Area => Perimeter * Apothema / 2;

        public double Perimeter => EdgeCount * edge;

        public abstract int EdgeCount { get; }
    }

    class Hexagon : RegularPolygon
    {
        public Hexagon(double edge) : base(edge)
        {
        }

        public override int EdgeCount => 6;
    }

    class Pentagon : RegularPolygon
    {
        public Pentagon(double edge) : base(edge)
        {
        }

        public override int EdgeCount => 5;
    }

    class Person
    {
        static int lastId = 0;
        int Id { get; }
        string Name { get; }

        private Person(string name)
        {
            Id = ++lastId;
            Name = name;
        }

        public override string ToString()
        {
            return $"Person {this.Id} {Name}";
        }

        public static Person CreateRandom()
        {
            return new Person("fhsdshksdhkfjsdhkhkgs");
        }

        public static Person Create(string name)
        {
            return new Person(name);
        }
    }
}
