﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelWorking.Threading
{
    class RaceConditionsSample
    {
        const int threadCount = 20;
        const int incrementsPerThread = 1000;

        internal static void Run()
        {
            Counter counter = new Counter();
            for (int i = 0; i < threadCount; i++)
            {
                Thread t = new Thread(counter.Execute) { Name = $"Thread # {i}" };
                t.Start(incrementsPerThread);
            }
            Thread.Sleep(23000);
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine($"Expected {threadCount * incrementsPerThread} actual {counter.Value} actual sync {counter.SyncValue}");
        }

        private static Random Rnd = new Random();
        
        public static void CounterWithRaceConditions() {
            var counter = new Counter2();
            const int threadCount = 20;
            const int incrementsPerThread = 1000;

            var threads = new List<Thread>();
            var before = DateTimeOffset.Now;
            for (int i = 0; i < threadCount; i++) {
                var t = new Thread(() => {
                    //Thread.Sleep(Random.Next(150));
                    Console.WriteLine($"Current thread {Thread.CurrentThread.ManagedThreadId}");
                    for (int j = 0; j < incrementsPerThread; j++) {
                        counter.Increment();
                        Thread.Sleep(Rnd.Next(2));
                    }
                });
                threads.Add(t);
                t.Start();
            }
            
            foreach(var t in threads) {
                t.Join();
            }
            var after = DateTimeOffset.Now;
            var elapsed = after - before;
            Console.WriteLine($"Value is {counter.Value}. Safe value is {counter.SafeValue}. Expected value was {incrementsPerThread * threadCount}. Elapsed {elapsed}");
        }
    }

    class Counter
    {
        private static Random random = new Random();
        private int counter;
        private int syncCounter;

        private object SyncRoot1 = new object();

        public Counter()
        {
            counter = 0;
            syncCounter = 0;
        }

        public void Increment()
        {
            counter++;
            lock (SyncRoot1)
            {
                syncCounter++;
            }
            //lock (x)
            //{
            //    DoSomething();
            //}
            // is equivalent to
            //System.Object obj = (System.Object)x;
            //System.Threading.Monitor.Enter(obj);
            //try
            //{
            //    DoSomething();
            //}
            //finally
            //{
            //    System.Threading.Monitor.Exit(obj);
            //}
        }

        public int Value => counter;
        public int SyncValue => syncCounter;

        internal void Execute(object param)
        {
            int incrementsPerThread = (int)param;
            
            for (int i = 0; i < incrementsPerThread; i++)
            {
                Increment();
                Thread.Sleep(random.Next(2));
            }
        }
    }
    
    class Counter2 {
        private int value = 0;
        private int safeValue = 0;
        public int Value => value;
        public int SafeValue => safeValue;

        private object syncRoot = new object();
        private object syncRoot2 = new object();


        /*
         * private object SyncRoot1 = new object();
         
          counter++;
        lock (SyncRoot1)
        {
            syncCounter++;
        }
         
         */

        public void Increment() {
            value++;
            
            lock (syncRoot)
            {
                safeValue++;
            }

            // var result = Value + 1;
            // Value = result;

            // value = 10
            // T0: Stack-T0 result = 11, Value = 10
            // T1: Stack-T1 result = 11, Value = 10
            // T1: Stack-T1 result = 11, Value = 11
            // T1 completed Value = 11
            // T0: Stack-T0 result = 11, Value = 11
            // T0: completed Value = 11
        }
    }
}
