﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelWorking.Threading
{
    class MonitorSyncSample
    {
        private static readonly object SyncRoot = new object();

        internal static void Run()
        {
            Thread t1 = new Thread(Task("Pippo"));
            Thread t2 = new Thread(Task("Pluto"));
            Thread t3 = new Thread(Task("Paperino", 200));
            Thread t4 = new Thread(Task("Topolino"));
            Thread t5 = new Thread(() => {
                Console.WriteLine("Unlocker thread");
                lock (SyncRoot)
                {
                    Thread.Sleep(1500);
                    //Console.WriteLine("Pulse...");
                    //Monitor.Pulse(SyncRoot);
                }
                Thread.Sleep(1500);
                lock (SyncRoot) {
                    Console.WriteLine("Pulse All...");
                    Monitor.PulseAll(SyncRoot);
                }                
            });
            t1.Start();
            t2.Start();
            t3.Start();
            t4.Start();
            t5.Start();
        }

        private static ThreadStart Task(string name, int? timeout = null)
        {
            return () => {
                lock (SyncRoot) {
                    Console.WriteLine($"Start task {name}");
                    if (timeout.HasValue) {
                        Monitor.Wait(SyncRoot, timeout.Value);
                    } else {
                        Monitor.Wait(SyncRoot);
                    }
                    Console.WriteLine($"End task {name}");
                }                
            };
        }
    }
}
