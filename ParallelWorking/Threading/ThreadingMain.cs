﻿using System;
using System.Threading;

namespace ParallelWorking.Threading
{
    // System.Threading
    //public delegate void ParameterizedThreadStart(
    //    object obj
    //)

    //public delegate void ThreadStart()

    class ThreadingMain
    {
        private const int CharCount = 10000;
        private static bool go = true;
        private static char c = 'x';
        public static void Run()
        {
            //RunSimpleThreads();
            // RaceConditionsSample.Run();
            RaceConditionsSample.CounterWithRaceConditions();
            // MonitorSyncSample.Run();
            //Console.WriteLine("Press any key to exit");
            //Console.ReadKey();
        }

        internal static void RunSimpleThreads()
        {
            Thread t1 = new Thread(WriteX);
            Thread t2 = new Thread(WriteY);
            t2.Name = "Second";
            t2.Priority = ThreadPriority.Lowest;
            Thread t3 = new Thread(WriteZ);
            t3.Name = "Third";
            t3.Priority = ThreadPriority.Highest;
            t1.Start();
            t2.Start();
            //Thread.Sleep(150);
            //c = 'a';
            //Thread.Sleep(150);
            go = false;
            t3.Start(/*"Param Value"*/);
        }

        internal static void WriteCurrentThreadInfo()
        {
            Console.WriteLine(Thread.CurrentThread.Name);
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine(Thread.CurrentThread.ThreadState);
            //Console.WriteLine(Thread.CurrentThread.);
        }

        internal static void WriteX()
        {
            WriteCurrentThreadInfo();
            for (int i = 0; i < CharCount; i++)
                //while (go)
            {
                Console.Write(c);
            }
        }

        internal static void WriteY()
        {
            WriteCurrentThreadInfo();
            for (int i = 0; i < CharCount; i++)
            {
                Console.Write("y");
            }
        }

        internal static void WriteZ(object arg)
        {
            WriteCurrentThreadInfo();
            Console.WriteLine(arg ?? "null");
            for (int i = 0; i < CharCount; i++)
            {
                Console.Write("z");
            }
        }
    }
}
