﻿using System;
using System.Threading.Tasks;

namespace ParallelWorking.AwaitAsync
{
    class ChainingMultipleTasks
    {
        internal static void Run()
        {
            Task<string> composedTask = GetFirstAsync()
                .ContinueWith(Log)
                .Unwrap()
                .ContinueWith(t => GetSecondAsync(t.Result))
                .Unwrap()
                .ContinueWith(Log)
                .Unwrap()
                .ContinueWith(t => GetThirdAsync(t.Result))
                .Unwrap()
                .ContinueWith(Log)
                .Unwrap()
                .ContinueWith(t => GetFourthAsync(t.Result))
                .Unwrap()
                .ContinueWith(Log)
                .Unwrap();
            composedTask.Wait();
            Console.WriteLine(composedTask.Result);
        }

        internal static void Run2()
        {
            Run2Async().Wait();
        }

        internal static async Task Run2Async()
        {
            string s1 = await GetFirstAsync();
            Log(s1);
            string s2 = await GetSecondAsync(s1);
            Log(s2);
            string s3 = await GetThirdAsync(s2);
            Log(s3);
            string s4 = await GetFourthAsync(s3);
            Log(s4);
            Console.WriteLine(s4);
        }

        internal static Task<string> Log(Task<string> t)
        {
            Console.WriteLine($"GOT {t.Result}");
            return Task.FromResult(t.Result);
        }

        internal static void Log(string s)
        {
            Console.WriteLine($"GOT {s}");
        }

        internal static async Task<string> GetFirstAsync()
        {
            await Task.Delay(500);
            return "First";
        }

        internal static async Task<string> GetSecondAsync(string s)
        {
            await Task.Delay(750);
            return "Second" + s;
        }

        internal static async Task<string> GetThirdAsync(string s)
        {
            await Task.Delay(1000);
            return "Third" + s;
        }

        internal static async Task<string> GetFourthAsync(string s)
        {
            await Task.Delay(1250);
            return "Fourth" + s;
        }
    }
}
