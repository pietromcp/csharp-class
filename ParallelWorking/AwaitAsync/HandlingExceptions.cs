﻿using System;
using System.Threading.Tasks;

namespace ParallelWorking.AwaitAsync
{
    class HandlingExceptions
    {
        internal static void Run()
        {
            RunAsync().Wait();
        }

        internal static void Run2()
        {
            try
            {
                ExecuteAsyncWithException().ContinueWith(t =>
                {
                    Console.WriteLine($"ERROR2: {t.Exception.Message} {t.Exception.GetType().Name}");
                    if (t.Exception != null)
                    {
                        foreach (var e in t.Exception.InnerExceptions)
                        {
                            Console.WriteLine($"INNER EXC {e.Message}");
                        }
                    }
                });
            }
            catch (Exception e)
            {
                Console.WriteLine($"ERROR2(catch): {e.Message} {e.GetType().Name}");
            }
        }

        internal static void Run3()
        {
            try
            {
                ExecuteAsyncWithException().Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine($"ERROR3(catch): {e.Message} {e.GetType().Name}");
            }
        }

        internal static async Task RunAsync()
        {
            try
            {
                await ExecuteAsyncWithException();
            }
            catch (Exception e)
            {
                Console.WriteLine($"ERROR: {e.Message}");
            }
        }

        internal static async Task ExecuteAsyncWithException()
        {
            await Task.Delay(250);
            throw new Exception("Exception in async method");
        }
    }
}
