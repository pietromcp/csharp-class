﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace PerformanceOptimization
{
    class Program
    {        
        static void Main(string[] args)
        {
            PopulateData();
            SearchData();
            UpdateData();
            CheckUpdatedData();
        }

        private static readonly DataContainer container = new DataContainer();

        private const int ItemsCount = 100000;

        private static readonly Random Rnd = new Random();

        static void PopulateData() {
            for (int i = 0; i < ItemsCount; i++) {
                container.InsertData($"item{i}", Rnd.Next(i + 1));
            }
        }

        static void SearchData() {
            var limit = ItemsCount / 4;
            var total = 0;
            for (int i = 0; i < limit; i++) {
                var item = container.SearchItem($"item{i}");
                if (item != null && item.Value % 10 == 1) {
                    total += item.Value;
                }
                if (i % 500 == 0) {
                    Console.WriteLine($"{i} - Current total is {total}");
                }
            }
            Console.WriteLine($"Final total is {total}");
        }

        static void UpdateData() {
            Console.WriteLine("Updating data...");
            var limit = ItemsCount / 4;
            var max = limit * 3;
            for (int i = 0; i < limit; i++)  {
                var name = $"item{Rnd.Next(max)}";
                if (i % 500 == 0) {
                    Console.WriteLine($"{i} - Updating data item {name}");
                }
                container.UpdateData(name, -1);
            }
        }

        static void CheckUpdatedData() {
            Console.WriteLine($"Count items having value == -1");
            var count = container.CountValue(-1);
            Console.WriteLine($"Updated item count is {count}");
        }
    }

    class DataContainer {
        private readonly IList<DataItem> data = new List<DataItem>();

        public void InsertData(string name, int value) {
            data.Add(new DataItem { Name = name, Value = value });
        }

        public DataItem SearchItem(string name) {
            foreach (var item in data) {
                if (item.Name == name) {
                    return item;
                }
            }
            return null;
        }

        public void UpdateData(string name, int value) {
            var item = SearchItem(name);
            if (item != null) {
                item.Value = value;
            }
            else {
                InsertData(name, value);
            }
        }

        public int CountValue(int value) {
            return data.Count(x => x.Value == value);
        }
    }

    class DataItem { 
        public string Name { get; set; }
        public int Value { get; set; }
    }
}
